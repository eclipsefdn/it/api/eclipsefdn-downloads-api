CREATE TABLE download_file_index (
  file_id int NOT NULL AUTO_INCREMENT,
  file_name varchar(255) NOT NULL DEFAULT '',
  download_count int NOT NULL DEFAULT 0,
  timestamp_disk bigint NOT NULL DEFAULT 0,
  size_disk_bytes bigint NOT NULL DEFAULT 0,
  md5sum char(32) DEFAULT NULL,
  sha1sum char(40) DEFAULT NULL,
  sha512sum char(128) DEFAULT NULL
);
INSERT INTO download_file_index(file_id, file_name, download_count, timestamp_disk, size_disk_bytes, md5sum, sha1sum, sha512sum)
  VALUES (1, 'sample_file.zip',123456, 1642175700000, 123456789, 'md5sum_sample', 'sha1sum_sample', 'sha512sum_sample');