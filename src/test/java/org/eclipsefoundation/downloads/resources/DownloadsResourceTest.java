/*
 * Copyright (C) 2022 Eclipse Foundation.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *          Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
 * 
 * SPDX-License-Identifier: EPL-2.0
*/
package org.eclipsefoundation.downloads.resources;

import org.eclipsefoundation.downloads.test.helper.SchemaNamespaceHelper;
import org.eclipsefoundation.testing.helpers.TestCaseHelper;
import org.eclipsefoundation.testing.models.EndpointTestBuilder;
import org.eclipsefoundation.testing.models.EndpointTestCase;
import org.eclipsefoundation.utils.helper.DateTimeHelper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;

import io.quarkus.test.junit.QuarkusTest;

@TestInstance(Lifecycle.PER_CLASS)
@QuarkusTest
class DownloadsResourceTest {

    private static final String LAST_YEAR = getLastYear();
    public static final String DOWNLOADS_BASE_URL = "";
    public static final String FILE_URL = DOWNLOADS_BASE_URL + "/file";
    public static final String FILE_BY_ID_URL = FILE_URL + "/{id}";

    public static final String RELEASE_BASE_URL = DOWNLOADS_BASE_URL + "/releases";

    public static final String LEGACY_RELEASE_URL = DOWNLOADS_BASE_URL + "/release/{releaseType}";
    public static final String LEGACY_RELEASE_NAME_URL = LEGACY_RELEASE_URL + "?release_name={param}";
    public static final String LEGACY_RELEASE_VERSION_URL = LEGACY_RELEASE_URL + "?release_version={param}";
    public static final String LEGACY_RELEASE_NAME_VERSION_URL = LEGACY_RELEASE_URL + "?release_name={param1}&release_version={param2}";

    public static final String RELEASES_URL = RELEASE_BASE_URL + "/{releaseType}/{releaseName}";
    public static final String RELEASE_VERSION_URL = RELEASES_URL + "/{releaseVersion}";

    /*
     * FILE BY ID
     */
    public static final EndpointTestCase FILE_BY_ID_SUCCESS = TestCaseHelper
            .buildSuccessCase(FILE_BY_ID_URL, new String[] { "1" }, SchemaNamespaceHelper.FILE_SCHEMA_PATH);

    public static final EndpointTestCase FILE_BY_ID_NOT_FOUND = TestCaseHelper
            .buildNotFoundCase(FILE_BY_ID_URL, new String[] { "99999" }, SchemaNamespaceHelper.ERROR_SCHEMA_PATH);

    /*
     * LEGACY RELEASE
     */
    public static final EndpointTestCase LEGACY_RELEASE_SUCCESS_NAME = TestCaseHelper
            .buildSuccessCase(LEGACY_RELEASE_NAME_URL, new String[] { "epp", LAST_YEAR + "-12" },
                    SchemaNamespaceHelper.RELEASES_SCHEMA_PATH);

    public static final EndpointTestCase LEGACY_RELEASE_SUCCESS_NAME_VERSION = TestCaseHelper
            .buildSuccessCase(LEGACY_RELEASE_NAME_VERSION_URL, new String[] { "epp", LAST_YEAR + "-12", "r" },
                    SchemaNamespaceHelper.RELEASE_SCHEMA_PATH);

    public static final EndpointTestCase LEGACY_RELEASE_INVALID_TYPE = TestCaseHelper
            .buildBadRequestCase(LEGACY_RELEASE_NAME_VERSION_URL, new String[] { "new", LAST_YEAR + "-12", "r" },
                    SchemaNamespaceHelper.ERROR_SCHEMA_PATH);

    public static final EndpointTestCase LEGACY_RELEASE_NO_NAME = TestCaseHelper
            .buildSuccessCase(LEGACY_RELEASE_URL, new String[] { "eclipse_packages" }, SchemaNamespaceHelper.RELEASE_SCHEMA_PATH);

    public static final EndpointTestCase LEGACY_RELEASE_NO_NAME_VERSION = TestCaseHelper
            .buildBadRequestCase(LEGACY_RELEASE_VERSION_URL, new String[] { "eclipse_packages", "r" },
                    SchemaNamespaceHelper.ERROR_SCHEMA_PATH);

    public static final EndpointTestCase LEGACY_RELEASE_NAME_NOT_FOUND = TestCaseHelper
            .buildNotFoundCase(LEGACY_RELEASE_NAME_URL, new String[] { "eclipse_packages", "nitro" },
                    SchemaNamespaceHelper.ERROR_SCHEMA_PATH);

    public static final EndpointTestCase LEGACY_RELEASE_VERSION_NOT_FOUND = TestCaseHelper
            .buildNotFoundCase(LEGACY_RELEASE_NAME_VERSION_URL, new String[] { "eclipse_packages", "epp", "nitro" },
                    SchemaNamespaceHelper.ERROR_SCHEMA_PATH);

    /*
     * GET RELEASES
     */
    public static final EndpointTestCase GET_RELEASES_SUCCESS = TestCaseHelper
            .buildSuccessCase(RELEASES_URL, new String[] { "epp", LAST_YEAR + "-06" }, SchemaNamespaceHelper.RELEASES_SCHEMA_PATH);

    public static final EndpointTestCase GET_RELEASES_NOT_FOUND = TestCaseHelper
            .buildNotFoundCase(RELEASES_URL, new String[] { "epp", "nitro" }, SchemaNamespaceHelper.ERROR_SCHEMA_PATH);

    public static final EndpointTestCase GET_RELEASES_INVALID_TYPE = TestCaseHelper
            .buildBadRequestCase(RELEASES_URL, new String[] { "new", LAST_YEAR + "-06" }, SchemaNamespaceHelper.ERROR_SCHEMA_PATH);

    /*
     * GET RELEASE VERSION
     */
    public static final EndpointTestCase GET_RELEASE_VERSION_SUCCESS = TestCaseHelper
            .buildSuccessCase(RELEASE_VERSION_URL, new String[] { "eclipse_packages", LAST_YEAR + "-06", "r" },
                    SchemaNamespaceHelper.RELEASE_SCHEMA_PATH);

    public static final EndpointTestCase GET_RELEASE_VERSION_NOT_FOUND_NAME = TestCaseHelper
            .buildNotFoundCase(RELEASE_VERSION_URL, new String[] { "eclipse_packages", "nitro", "m1" },
                    SchemaNamespaceHelper.ERROR_SCHEMA_PATH);

    public static final EndpointTestCase GET_RELEASE_VERSION_NOT_FOUND_VERSION = TestCaseHelper
            .buildNotFoundCase(RELEASE_VERSION_URL, new String[] { "epp", "2020-03", "version1" }, SchemaNamespaceHelper.ERROR_SCHEMA_PATH);

    public static final EndpointTestCase GET_RELEASE_VERSION_INVALID_TYPE = TestCaseHelper
            .buildBadRequestCase(RELEASE_VERSION_URL, new String[] { "new", LAST_YEAR + "-06", "r" },
                    SchemaNamespaceHelper.ERROR_SCHEMA_PATH);

    /*
     * FILE BY ID
     */
    @Test
    void getFileByID_success() {
        EndpointTestBuilder.from(FILE_BY_ID_SUCCESS).run();
    }

    @Test
    void getFileByID_success_validSchema() {
        EndpointTestBuilder.from(FILE_BY_ID_SUCCESS).andCheckSchema().run();
    }

    @Test
    void getFileByID_success_validResponseFormat() {
        EndpointTestBuilder.from(FILE_BY_ID_SUCCESS).andCheckFormat().run();
    }

    @Test
    void getFileByID_failure_notFound() {
        EndpointTestBuilder.from(FILE_BY_ID_NOT_FOUND).run();
    }

    @Test
    void getFileByID_failure_notFound_validSchema() {
        EndpointTestBuilder.from(FILE_BY_ID_NOT_FOUND).andCheckSchema().run();
    }

    /*
     * LEGACY RELEASE
     */
    @Test
    void getReleaseLegacy_success() {
        EndpointTestBuilder.from(LEGACY_RELEASE_SUCCESS_NAME).run();
        EndpointTestBuilder.from(LEGACY_RELEASE_SUCCESS_NAME_VERSION).run();
    }

    @Test
    void getReleaseLegacy_success_validSchema() {
        EndpointTestBuilder.from(LEGACY_RELEASE_SUCCESS_NAME).andCheckSchema().run();
        EndpointTestBuilder.from(LEGACY_RELEASE_SUCCESS_NAME_VERSION).andCheckSchema().run();
    }

    @Test
    void getReleaseLegacy_success_validResponseFormat() {
        EndpointTestBuilder.from(LEGACY_RELEASE_SUCCESS_NAME).andCheckFormat().run();
        EndpointTestBuilder.from(LEGACY_RELEASE_SUCCESS_NAME_VERSION).andCheckFormat().run();
    }

    @Test
    void getReleaseLegacy_failure_missingReleaseName() {
        EndpointTestBuilder.from(LEGACY_RELEASE_NO_NAME).run();
        EndpointTestBuilder.from(LEGACY_RELEASE_NO_NAME_VERSION).run();
    }

    @Test
    void getReleaseLegacy_failure_invalidReleaseType() {
        EndpointTestBuilder.from(LEGACY_RELEASE_INVALID_TYPE).run();
    }

    @Test
    void getReleaseLegacy_failure_invalidReleaseType_validSchema() {
        EndpointTestBuilder.from(LEGACY_RELEASE_INVALID_TYPE).andCheckSchema().run();
    }

    @Test
    void getReleaseLEgacy_failure_nameNotFound() {
        EndpointTestBuilder.from(LEGACY_RELEASE_NAME_NOT_FOUND).run();
    }

    @Test
    void getReleaseLEgacy_failure_nameNotFound_validSchema() {
        EndpointTestBuilder.from(LEGACY_RELEASE_NAME_NOT_FOUND).andCheckSchema().run();
    }

    @Test
    void getReleaseLEgacy_failure_versionNotFound() {
        EndpointTestBuilder.from(LEGACY_RELEASE_VERSION_NOT_FOUND).run();
    }

    @Test
    void getReleaseLEgacy_failure_versionNotFound_validSchema() {
        EndpointTestBuilder.from(LEGACY_RELEASE_VERSION_NOT_FOUND).andCheckSchema().run();
    }

    /*
     * GET RELEASE
     */
    @Test
    void getRelease_success() {
        EndpointTestBuilder.from(GET_RELEASES_SUCCESS).run();
    }

    @Test
    void getRelease_success_validSchema() {
        EndpointTestBuilder.from(GET_RELEASES_SUCCESS).andCheckSchema().run();
    }

    @Test
    void getRelease_success_validResponseFormat() {
        EndpointTestBuilder.from(GET_RELEASES_SUCCESS).andCheckFormat().run();
    }

    @Test
    void getRelease_failure_notFound() {
        EndpointTestBuilder.from(GET_RELEASES_NOT_FOUND).run();
    }

    @Test
    void getRelease_failure_notFound_validSchema() {
        EndpointTestBuilder.from(GET_RELEASES_NOT_FOUND).andCheckSchema().run();
    }

    @Test
    void getRelease_failure_invalidReleaseType() {
        EndpointTestBuilder.from(GET_RELEASES_INVALID_TYPE).run();
    }

    @Test
    void getRelease_failure_invalidReleaseType_validSchema() {
        EndpointTestBuilder.from(GET_RELEASES_INVALID_TYPE).andCheckSchema().run();
    }

    /*
     * GET RELEASE VERSION
     */
    @Test
    void getReleaseVersion_success() {
        EndpointTestBuilder.from(GET_RELEASE_VERSION_SUCCESS).run();
    }

    @Test
    void getReleaseVersion_success_validSchema() {
        EndpointTestBuilder.from(GET_RELEASE_VERSION_SUCCESS).andCheckSchema().run();
    }

    @Test
    void getReleaseVersion_success_validResponseFormat() {
        EndpointTestBuilder.from(GET_RELEASE_VERSION_SUCCESS).andCheckFormat().run();
    }

    @Test
    void getReleaseVersion_failure_nameNotFound() {
        EndpointTestBuilder.from(GET_RELEASE_VERSION_NOT_FOUND_NAME).run();
    }

    @Test
    void getReleaseVersion_failure_nameNotFound_validSchema() {
        EndpointTestBuilder.from(GET_RELEASE_VERSION_NOT_FOUND_NAME).andCheckSchema().run();
    }

    @Test
    void getReleaseVersion_failure_versionNotFound() {
        EndpointTestBuilder.from(GET_RELEASE_VERSION_NOT_FOUND_VERSION).run();
    }

    @Test
    void getReleaseVersion_failure_versionNotFound_validSchema() {
        EndpointTestBuilder.from(GET_RELEASE_VERSION_NOT_FOUND_VERSION).andCheckSchema().run();
    }

    @Test
    void getReleaseVersion_failure_invalidReleaseType() {
        EndpointTestBuilder.from(GET_RELEASE_VERSION_INVALID_TYPE).run();
    }

    @Test
    void getReleaseVersion_failure_invalidReleaseType_validSchema() {
        EndpointTestBuilder.from(GET_RELEASE_VERSION_INVALID_TYPE).andCheckSchema().run();
    }

    private static String getLastYear() {
        return Integer.toString(DateTimeHelper.now().getYear() - 1);
    }
}
