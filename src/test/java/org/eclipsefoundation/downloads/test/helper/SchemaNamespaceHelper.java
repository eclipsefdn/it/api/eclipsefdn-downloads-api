/*
 * Copyright (C) 2022 Eclipse Foundation.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *          Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
 * 
 * SPDX-License-Identifier: EPL-2.0
*/
package org.eclipsefoundation.downloads.test.helper;

public final class SchemaNamespaceHelper {
    public static final String BASE_SCHEMAS_PATH = "schemas/";
    public static final String BASE_SCHEMAS_PATH_SUFFIX = "-schema.json";
    public static final String FILES_SCHEMA_PATH = BASE_SCHEMAS_PATH + "files" + BASE_SCHEMAS_PATH_SUFFIX;
    public static final String FILE_SCHEMA_PATH = BASE_SCHEMAS_PATH + "file" + BASE_SCHEMAS_PATH_SUFFIX;
    public static final String RELEASES_SCHEMA_PATH = BASE_SCHEMAS_PATH + "releases" + BASE_SCHEMAS_PATH_SUFFIX;
    public static final String RELEASE_SCHEMA_PATH = BASE_SCHEMAS_PATH + "release" + BASE_SCHEMAS_PATH_SUFFIX;
    public static final String ERROR_SCHEMA_PATH = BASE_SCHEMAS_PATH + "error" + BASE_SCHEMAS_PATH_SUFFIX;
}
