/*
 * Copyright (C) 2022 Eclipse Foundation.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 * 
 * SPDX-License-Identifier: EPL-2.0
*/
package org.eclipsefoundation.downloads.dto;

import java.util.Objects;

import org.apache.commons.lang3.StringUtils;
import org.eclipsefoundation.downloads.namespaces.DownloadsUrlParameterNames;
import org.eclipsefoundation.http.namespace.DefaultUrlParameterNames;
import org.eclipsefoundation.persistence.dto.BareNode;
import org.eclipsefoundation.persistence.dto.filter.DtoFilter;
import org.eclipsefoundation.persistence.model.DtoTable;
import org.eclipsefoundation.persistence.model.ParameterizedSQLStatement;
import org.eclipsefoundation.persistence.model.ParameterizedSQLStatementBuilder;

import com.fasterxml.jackson.annotation.JsonIgnore;

import jakarta.inject.Inject;
import jakarta.inject.Singleton;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.validation.constraints.NotNull;
import jakarta.ws.rs.core.MultivaluedMap;

/**
 * DTO for the `download_file_index` DB entity
 */
@Entity
@Table(name = "download_file_index")
public class DownloadFileIndex extends BareNode {
    public static final DtoTable TABLE = new DtoTable(DownloadFileIndex.class, "dfi");

    @Id
    private int fileId;
    @NotNull
    private String fileName;
    @NotNull
    private int downloadCount;
    @NotNull
    private long timestampDisk;
    @NotNull
    private int sizeDiskBytes;
    private String md5sum;
    private String sha1sum;
    private String sha512sum;

    @JsonIgnore
    @Override
    public Object getId() {
        return getFileId();
    }

    /**
     * @return the fileId
     */
    public int getFileId() {
        return fileId;
    }

    /**
     * @param fileId the fileId to set
     */
    public void setFileId(int fileId) {
        this.fileId = fileId;
    }

    /**
     * @return the fileName
     */
    public String getFileName() {
        return fileName;
    }

    /**
     * @param fileName the fileName to set
     */
    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    /**
     * @return the downloadCount
     */
    public int getDownloadCount() {
        return downloadCount;
    }

    /**
     * @param downloadCount the downloadCount to set
     */
    public void setDownloadCount(int downloadCount) {
        this.downloadCount = downloadCount;
    }

    /**
     * @return the timestampDisk
     */
    public long getTimestampDisk() {
        return timestampDisk;
    }

    /**
     * @param timestampDisk the timestampDisk to set
     */
    public void setTimestampDisk(long timestampDisk) {
        this.timestampDisk = timestampDisk;
    }

    /**
     * @return the sizeDiskBytes
     */
    public int getSizeDiskBytes() {
        return sizeDiskBytes;
    }

    /**
     * @param sizeDiskBytes the sizeDiskBytes to set
     */
    public void setSizeDiskBytes(int sizeDiskBytes) {
        this.sizeDiskBytes = sizeDiskBytes;
    }

    /**
     * @return the md5sum
     */
    public String getMd5sum() {
        return md5sum;
    }

    /**
     * @param md5sum the md5sum to set
     */
    public void setMd5sum(String md5sum) {
        this.md5sum = md5sum;
    }

    /**
     * @return the sha1sum
     */
    public String getSha1sum() {
        return sha1sum;
    }

    /**
     * @param sha1sum the sha1sum to set
     */
    public void setSha1sum(String sha1sum) {
        this.sha1sum = sha1sum;
    }

    /**
     * @return the sha512sum
     */
    public String getSha512sum() {
        return sha512sum;
    }

    /**
     * @param sha512sum the sha512sum to set
     */
    public void setSha512sum(String sha512sum) {
        this.sha512sum = sha512sum;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + Objects
                .hash(downloadCount, fileId, fileName, md5sum, sha1sum, sha512sum, timestampDisk, sizeDiskBytes);
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!super.equals(obj)) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        DownloadFileIndex other = (DownloadFileIndex) obj;
        return downloadCount == other.downloadCount && fileId == other.fileId && Objects.equals(fileName, other.fileName)
                && Objects.equals(md5sum, other.md5sum) && Objects.equals(sha1sum, other.sha1sum)
                && Objects.equals(sha512sum, other.sha512sum) && timestampDisk == other.timestampDisk
                && sizeDiskBytes == other.sizeDiskBytes;
    }

    @Singleton
    public static class DownloadFileIndexFilter implements DtoFilter<DownloadFileIndex> {
        @Inject
        ParameterizedSQLStatementBuilder builder;

        @Override
        public ParameterizedSQLStatement getFilters(MultivaluedMap<String, String> params, boolean isRoot) {
            ParameterizedSQLStatement stmt = builder.build(TABLE);
            if (isRoot) {
                // ID check
                String id = params.getFirst(DefaultUrlParameterNames.ID.getName());
                if (StringUtils.isNumeric(id)) {
                    stmt
                            .addClause(new ParameterizedSQLStatement.Clause(TABLE.getAlias() + ".fileId = ?",
                                    new Object[] { Integer.valueOf(id) }));
                }
                // file name check
                String fileName = params.getFirst(DownloadsUrlParameterNames.FILE_NAME_VALUE);
                if (StringUtils.isNotBlank(fileName)) {
                    stmt.addClause(new ParameterizedSQLStatement.Clause(TABLE.getAlias() + ".fileName = ?", new Object[] { fileName }));
                }
            }
            return stmt;
        }

        @Override
        public Class<DownloadFileIndex> getType() {
            return DownloadFileIndex.class;
        }
    }
}
