/*
 * Copyright (C) 2022 Eclipse Foundation.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 * 
 * SPDX-License-Identifier: EPL-2.0
*/
package org.eclipsefoundation.downloads.namespaces;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipsefoundation.utils.namespace.UrlParameterNamespace;

import jakarta.inject.Singleton;

/**
 * Helper class used to provide parameter names througout the application.
 */
@Singleton
public final class DownloadsUrlParameterNames implements UrlParameterNamespace {
	public static final String RELEASE_NAME_VALUE = "release_name";
	public static final String RELEASE_VERSION_VALUE = "release_version";
	public static final String FILE_NAME_VALUE = "file_name";
	public static final String MIRROR_ID_VALUE = "file_name";
	public static final String REMOTE_HOST_VALUE = "file_name";
	public static final String REMOTE_ADDR_VALUE = "file_name";
	public static final String CCODE_VALUE = "file_name";

	public static final UrlParameter FILE_NAME = new UrlParameter(FILE_NAME_VALUE);
	public static final UrlParameter RELEASE_NAME = new UrlParameter(RELEASE_NAME_VALUE);
	public static final UrlParameter RELEASE_VERSION = new UrlParameter(RELEASE_VERSION_VALUE);
	public static final UrlParameter MIRROR_ID = new UrlParameter(MIRROR_ID_VALUE);
	public static final UrlParameter REMOTE_HOST = new UrlParameter(REMOTE_HOST_VALUE);
	public static final UrlParameter REMOTE_ADDR = new UrlParameter(REMOTE_ADDR_VALUE);
	public static final UrlParameter CCODE = new UrlParameter(CCODE_VALUE);

	private static final List<UrlParameter> params = Collections.unmodifiableList(
			Arrays.asList(FILE_NAME, RELEASE_NAME, RELEASE_VERSION, MIRROR_ID, REMOTE_HOST, REMOTE_ADDR, CCODE));

	@Override
	public List<UrlParameter> getParameters() {
		return new ArrayList<>(params);
	}
}