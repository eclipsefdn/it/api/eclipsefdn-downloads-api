/*
 * Copyright (C) 2022 Eclipse Foundation.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 * 
 * SPDX-License-Identifier: EPL-2.0
*/
package org.eclipsefoundation.downloads.models;

import java.util.List;

import jakarta.annotation.Nullable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.google.auto.value.AutoValue;

/**
 * Serialization for input/output of the release version packages.
 * 
 * @author Martin Lowe
 *
 */
@AutoValue
@JsonDeserialize(builder = AutoValue_ReleaseVersionPackages.Builder.class)
public abstract class ReleaseVersionPackages {
    public abstract String getReleaseName();

    public abstract String getReleaseVersion();

    public abstract ReleaseTrackerPackages getPackages();

    public static Builder builder() {
        return new AutoValue_ReleaseVersionPackages.Builder();
    }

    @AutoValue.Builder
    @JsonPOJOBuilder(withPrefix = "set")
    public abstract static class Builder {
        public abstract Builder setReleaseName(String releaseName);

        public abstract Builder setReleaseVersion(String releaseVersion);

        public abstract Builder setPackages(ReleaseTrackerPackages packages);

        public abstract ReleaseVersionPackages build();
    }

    @AutoValue
    @JsonDeserialize(builder = AutoValue_ReleaseVersionPackages_ReleaseTrackerPackages.Builder.class)
    public abstract static class ReleaseTrackerPackages {
        @Nullable
        @JsonProperty("java-package")
        public abstract ReleaseTrackerPackage getJavaPackage();

        @Nullable
        @JsonProperty("jee-package")
        public abstract ReleaseTrackerPackage getJEEPackage();

        @Nullable
        @JsonProperty("cpp-package")
        public abstract ReleaseTrackerPackage getCPPPackage();

        @Nullable
        @JsonProperty("committers-package")
        public abstract ReleaseTrackerPackage getCommittersPackage();

        @Nullable
        @JsonProperty("php-package")
        public abstract ReleaseTrackerPackage getPHPPackage();

        @Nullable
        @JsonProperty("dsl-package")
        public abstract ReleaseTrackerPackage getDSLPackage();

        @Nullable
        @JsonProperty("embedcpp-package")
        public abstract ReleaseTrackerPackage getEmbeddedCPPPackage();

        @Nullable
        @JsonProperty("modeling-package")
        public abstract ReleaseTrackerPackage getModelingPackage();

        @Nullable
        @JsonProperty("rcp-package")
        public abstract ReleaseTrackerPackage getRCPPackage();

        @JsonInclude(Include.NON_NULL)
        @Nullable
        @JsonProperty("parallel-package")
        public abstract ReleaseTrackerPackage getParallelPackage();

        @Nullable
        @JsonProperty("scout-package")
        public abstract ReleaseTrackerPackage getScoutPackage();

        public static Builder builder() {
            return new AutoValue_ReleaseVersionPackages_ReleaseTrackerPackages.Builder();
        }

        @AutoValue.Builder
        @JsonPOJOBuilder(withPrefix = "set")
        public abstract static class Builder {
            @JsonProperty("java-package")
            public abstract Builder setJavaPackage(@Nullable ReleaseTrackerPackage javaPackage);

            @JsonProperty("jee-package")
            public abstract Builder setJEEPackage(@Nullable ReleaseTrackerPackage jeePackage);

            @JsonProperty("cpp-package")
            public abstract Builder setCPPPackage(@Nullable ReleaseTrackerPackage cppPackage);

            @JsonProperty("committers-package")
            public abstract Builder setCommittersPackage(@Nullable ReleaseTrackerPackage committersPackage);

            @JsonProperty("php-package")
            public abstract Builder setPHPPackage(@Nullable ReleaseTrackerPackage phpPackage);

            @JsonProperty("dsl-package")
            public abstract Builder setDSLPackage(@Nullable ReleaseTrackerPackage dslPackage);

            @JsonProperty("embedcpp-package")
            public abstract Builder setEmbeddedCPPPackage(@Nullable ReleaseTrackerPackage embeddedCPPPackage);

            @JsonProperty("modeling-package")
            public abstract Builder setModelingPackage(@Nullable ReleaseTrackerPackage modelingPackage);

            @JsonProperty("rcp-package")
            public abstract Builder setRCPPackage(@Nullable ReleaseTrackerPackage rcpPackage);

            @JsonProperty("parallel-package")
            public abstract Builder setParallelPackage(@Nullable ReleaseTrackerPackage parallelPackage);

            @JsonProperty("scout-package")
            public abstract Builder setScoutPackage(@Nullable ReleaseTrackerPackage scoutPackage);

            public abstract ReleaseTrackerPackages build();
        }
    }

    @AutoValue
    @JsonDeserialize(builder = AutoValue_ReleaseVersionPackages_ReleaseTrackerPackage.Builder.class)
    public abstract static class ReleaseTrackerPackage {
        public abstract String getName();

        public abstract String getPackageBugzillaId();

        public abstract String getDownloadCount();

        public abstract String getWebsiteUrl();

        public abstract Boolean getIncubating();

        @JsonProperty("class")
        public abstract String getClazz();

        public abstract String getBody();

        public abstract List<String> getFeatures();

        public abstract OSReleases getFiles();

        public static Builder builder() {
            return new AutoValue_ReleaseVersionPackages_ReleaseTrackerPackage.Builder();
        }

        @AutoValue.Builder
        @JsonPOJOBuilder(withPrefix = "set")
        public abstract static class Builder {
            public abstract Builder setName(String name);

            public abstract Builder setPackageBugzillaId(String packageBugzillaId);

            public abstract Builder setDownloadCount(String downloadCount);

            public abstract Builder setWebsiteUrl(String websiteUrl);

            public abstract Builder setIncubating(Boolean incubating);

            @JsonProperty("class")
            public abstract Builder setClazz(String clazz);

            public abstract Builder setBody(String body);

            public abstract Builder setFeatures(List<String> features);

            public abstract Builder setFiles(OSReleases files);

            public abstract ReleaseTrackerPackage build();
        }
    }

    @AutoValue
    @JsonDeserialize(builder = AutoValue_ReleaseVersionPackages_OSReleases.Builder.class)
    public abstract static class OSReleases {
        public abstract OSRelease getMac();

        public abstract OSRelease getWindows();

        public abstract OSRelease getLinux();

        public static Builder builder() {
            return new AutoValue_ReleaseVersionPackages_OSReleases.Builder();
        }

        @AutoValue.Builder
        @JsonPOJOBuilder(withPrefix = "set")
        public abstract static class Builder {
            public abstract Builder setMac(OSRelease mac);

            public abstract Builder setWindows(OSRelease windows);

            public abstract Builder setLinux(OSRelease linux);

            public abstract OSReleases build();
        }
    }

    @AutoValue
    @JsonDeserialize(builder = AutoValue_ReleaseVersionPackages_OSRelease.Builder.class)
    public abstract static class OSRelease {
        @Nullable
        @JsonProperty("32")
        public abstract ArchRelease getRelease32Bit();

        @JsonProperty("64")
        public abstract ArchRelease getRelease64Bit();

        public static Builder builder() {
            return new AutoValue_ReleaseVersionPackages_OSRelease.Builder();
        }

        @AutoValue.Builder
        @JsonPOJOBuilder(withPrefix = "set")
        public abstract static class Builder {
            @JsonProperty("32")
            public abstract Builder setRelease32Bit(@Nullable ArchRelease release32Bit);

            @JsonProperty("64")
            public abstract Builder setRelease64Bit(ArchRelease release64Bit);

            public abstract OSRelease build();
        }
    }

    @AutoValue
    @JsonDeserialize(builder = AutoValue_ReleaseVersionPackages_ArchRelease.Builder.class)
    public abstract static class ArchRelease {
        public abstract String getUrl();

        public abstract String getSize();

        @Nullable
        public abstract String getFileId();

        public abstract String getFileUrl();

        public abstract String getDownloadCount();

        public abstract Checksums getChecksum();

        public static Builder builder() {
            return new AutoValue_ReleaseVersionPackages_ArchRelease.Builder();
        }

        @AutoValue.Builder
        @JsonPOJOBuilder(withPrefix = "set")
        public abstract static class Builder {
            public abstract Builder setUrl(String url);

            public abstract Builder setSize(String size);

            public abstract Builder setFileId(@Nullable String fileId);

            public abstract Builder setFileUrl(String fileUrl);

            public abstract Builder setDownloadCount(String downloadCount);

            public abstract Builder setChecksum(Checksums checksum);

            public abstract ArchRelease build();
        }
    }

    @AutoValue
    @JsonDeserialize(builder = AutoValue_ReleaseVersionPackages_Checksums.Builder.class)
    public abstract static class Checksums {
        @Nullable
        public abstract String getMd5();

        @Nullable
        public abstract String getSha1();

        @Nullable
        public abstract String getSha512();

        public static Builder builder() {
            return new AutoValue_ReleaseVersionPackages_Checksums.Builder();
        }

        @AutoValue.Builder
        @JsonPOJOBuilder(withPrefix = "set")
        public abstract static class Builder {
            public abstract Builder setMd5(@Nullable String md5);

            public abstract Builder setSha1(@Nullable String sha1);

            public abstract Builder setSha512(@Nullable String sha512);

            public abstract Checksums build();
        }
    }

}
