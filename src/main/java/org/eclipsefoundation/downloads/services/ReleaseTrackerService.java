/*
 * Copyright (C) 2022 Eclipse Foundation.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 * 
 * SPDX-License-Identifier: EPL-2.0
*/
package org.eclipsefoundation.downloads.services;

import java.util.List;
import java.util.Optional;

import org.eclipsefoundation.downloads.models.TrackedReleases.Release;

/**
 * Service for retrieving tracked releases for the Eclipse project.
 * 
 * @author Martin Lowe
 *
 */
public interface ReleaseTrackerService {

    /**
     * Retrieves a release by its name (case-sensitive).
     * 
     * @param releaseName the name of the release to retrieve
     * @return an optional containing the release if found, otherwise an empty optional.
     */
    Optional<Release> getReleaseByName(String releaseName);

    /**
     * Gets all releases that have the is_current flag, denoting an active release.
     * 
     * @return all active releases.
     */
    List<Release> getActiveReleases();

}
