/*
 * Copyright (C) 2022 Eclipse Foundation.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
*/
package org.eclipsefoundation.downloads.services.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Optional;

import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.eclipsefoundation.caching.service.CachingService;
import org.eclipsefoundation.downloads.api.DrupalAPI;
import org.eclipsefoundation.downloads.models.TrackedReleases;
import org.eclipsefoundation.downloads.models.TrackedReleases.Release;
import org.eclipsefoundation.downloads.models.TrackedReleases.ReleaseVersion;
import org.eclipsefoundation.downloads.services.ReleaseTrackerService;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.ws.rs.core.MultivaluedHashMap;

/**
 * Default implementation of the release tracker service, using the Drupal API as the source of data for the releases.
 * 
 * @author Martin Lowe
 *
 */
@ApplicationScoped
public class DefaultReleaseTrackerService implements ReleaseTrackerService {

    private final CachingService cache;
    private final DrupalAPI api;

    /**
     * Default constructor for release tracker, ingesting the auto implementation of the Drupal API and the core caching service.
     * 
     * @param cache service for managing cached data
     * @param api interface for interacting with the Drupal API.
     */
    public DefaultReleaseTrackerService(CachingService cache, @RestClient DrupalAPI api) {
        this.api = api;
        this.cache = cache;
    }

    @Override
    public Optional<Release> getReleaseByName(String releaseName) {
        return Optional.ofNullable(getCachedReleases().getReleases().get(releaseName));
    }

    @Override
    public List<Release> getActiveReleases() {
        // Get only current releases
        return getCachedReleases()
                .getReleases()
                .values()
                .stream()
                .filter(r -> r.getVersions().stream().anyMatch(ReleaseVersion::getIsCurrent))
                .toList();
    }

    /**
     * Get the cached releases from the default location (Drupal API).
     * 
     * @return cached version of releases, or an empty object.
     */
    private TrackedReleases getCachedReleases() {
        return cache
                .get("all", new MultivaluedHashMap<>(), TrackedReleases.class, api::getTrackedReleases)
                .data()
                .orElse(TrackedReleases.builder().setReleases(new HashMap<>()).build());
    }
}
