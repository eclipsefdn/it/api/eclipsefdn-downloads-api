/*
 * Copyright (C) 2022 Eclipse Foundation.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 * 
 * SPDX-License-Identifier: EPL-2.0
*/
package org.eclipsefoundation.downloads.api;

import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;
import org.eclipsefoundation.downloads.models.ReleaseVersionPackages.ReleaseTrackerPackages;
import org.eclipsefoundation.downloads.models.TrackedReleases;

import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;

/**
 * Rest client bound to the drupal release API
 */
@RegisterRestClient(configKey = "drupal")
public interface DrupalAPI {

    /**
     * Get all tracked releases mapped by release name.
     * 
     * @return A TrackedReleases entity with all releases mapped
     */
    @GET
    @Path("downloads/packages/admin/release_tracker/json/")
    TrackedReleases getTrackedReleases();

    /**
     * Get all release packages for a given release name and version.
     * 
     * @param releaseName The given release name.
     * @param version The given release version.
     * @return A ReleaseTrackerPackage entity with all available packages.
     */
    @GET
    @Path("downloads/packages/admin/release_tracker/json/{releaseName}%20{version}/all")
    ReleaseTrackerPackages get(@PathParam("releaseName") String releaseName, @PathParam("version") String version);
}
