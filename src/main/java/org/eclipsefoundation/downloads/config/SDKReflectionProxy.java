package org.eclipsefoundation.downloads.config;

import org.eclipsefoundation.http.model.WebError;

import io.quarkus.runtime.annotations.RegisterForReflection;

/**
 * Temporary fix for reflection errors in SDK, should be removed for 1.0.2
 */
@RegisterForReflection(targets = { WebError.class })
public class SDKReflectionProxy {
}