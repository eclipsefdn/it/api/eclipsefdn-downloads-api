/*
 * Copyright (C) 2022 Eclipse Foundation.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *          Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
 * 
 * SPDX-License-Identifier: EPL-2.0
*/
package org.eclipsefoundation.downloads.resources;

import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;
import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.eclipsefoundation.caching.service.CachingService;
import org.eclipsefoundation.downloads.api.DrupalAPI;
import org.eclipsefoundation.downloads.dto.DownloadFileIndex;
import org.eclipsefoundation.downloads.models.ReleaseVersionPackages;
import org.eclipsefoundation.downloads.models.ReleaseVersionPackages.ReleaseTrackerPackages;
import org.eclipsefoundation.downloads.models.TrackedReleases.Release;
import org.eclipsefoundation.downloads.models.TrackedReleases.ReleaseVersion;
import org.eclipsefoundation.downloads.namespaces.DownloadsUrlParameterNames;
import org.eclipsefoundation.downloads.services.ReleaseTrackerService;
import org.eclipsefoundation.http.model.RequestWrapper;
import org.eclipsefoundation.http.model.WebError;
import org.eclipsefoundation.http.namespace.CacheControlCommonValues;
import org.eclipsefoundation.http.namespace.DefaultUrlParameterNames;
import org.eclipsefoundation.persistence.dao.impl.DefaultHibernateDao;
import org.eclipsefoundation.persistence.model.RDBMSQuery;
import org.eclipsefoundation.persistence.service.FilterService;
import org.jboss.resteasy.reactive.Cache;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jakarta.inject.Inject;
import jakarta.ws.rs.BadRequestException;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.NotFoundException;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.ServerErrorException;
import jakarta.ws.rs.WebApplicationException;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.MultivaluedHashMap;
import jakarta.ws.rs.core.MultivaluedMap;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.Response.Status;

/**
 * Resource class containing all endpoints related to Eclipse package release information. Provides endpoints for download files and release
 * packages.
 */
@Path("")
@Produces(MediaType.APPLICATION_JSON)
public class DownloadsResource {
    private static final Logger LOGGER = LoggerFactory.getLogger(DownloadsResource.class);

    private static final Pattern MODERN_RELEASE_PATTERN = Pattern.compile("^\\d");

    private static final String RELEASE_TYPE_ERROR_MESSAGE_FORMAT = "Invalid release type: %s";

    @Inject
    DefaultHibernateDao dao;
    @Inject
    FilterService filters;
    @Inject
    CachingService cache;
    @Inject
    RequestWrapper wrap;

    @Inject
    @RestClient
    DrupalAPI api;

    @Inject
    ReleaseTrackerService releaseTrackerService;

    /**
     * Provides information for a specified file. Accepts an 'id' param and returns a Response containing the desired
     * {@link DownloadFileIndex} entity if it exists. Throws a {@link NotFoundException} when the file cannot be found.
     * 
     * @param id The desired file id.
     * @return A 200 OK Response containg the desired file information. a 404 NotFound Response if the file cannot be found.
     */
    @GET
    @Path("file/{id}")
    @Cache(maxAge = CacheControlCommonValues.SAFE_CACHE_MAX_AGE)
    public Response byID(@PathParam("id") String id) {
        // filter by ID for the file index
        MultivaluedMap<String, String> params = new MultivaluedHashMap<>();
        params.add(DefaultUrlParameterNames.ID.getName(), id);

        // get the index, and make sure we have a result
        Optional<List<DownloadFileIndex>> dfis = cache
                .get(id, params, DownloadFileIndex.class,
                        () -> dao.get(new RDBMSQuery<>(wrap, filters.get(DownloadFileIndex.class), params)))
                .data();

        if (dfis.isEmpty() || dfis.get().isEmpty()) {
            throw new NotFoundException(String.format("No DownloadFileIndex found with id '%s'", id));
        }

        return Response.ok(dfis.get().get(0)).build();
    }

    /**
     * Provides information for a specific package release. The specific package is determined by using the provided 'releaseType',
     * 'releaseName', and 'releaseVersion' parameters. Returns a Response containing the desired release package if it exists. Throws a
     * {@link BadRequestException} if the releaseType is invalid or if the releaseVersion is provided without the releaseName.
     * 
     * @param releaseType The desired release type.
     * @param releaseName The desired release name.
     * @param releaseVersion The desired release version.
     * @return A 200 OK Response containing the desired ReleaseVersionPackages entity if it is avaiable. Throwing a 400 BadRequest if params
     * are invalid.
     */
    @GET
    @Path("release/{releaseType}")
    @Cache(maxAge = CacheControlCommonValues.AGGRESSIVE_CACHE_MAX_AGE)
    public Response packageRelease(@PathParam("releaseType") String releaseType,
            @QueryParam(DownloadsUrlParameterNames.RELEASE_NAME_VALUE) String releaseName,
            @QueryParam(DownloadsUrlParameterNames.RELEASE_VERSION_VALUE) String releaseVersion) {

        // Ensure release type is valid
        if (!isValidType(releaseType)) {
            throw new BadRequestException(String.format(RELEASE_TYPE_ERROR_MESSAGE_FORMAT, releaseType));
        }

        // use existing calls and route according to what query params are provided
        if (StringUtils.isNotBlank(releaseName) && StringUtils.isNotBlank(releaseVersion)) {
            return releaseVersion(releaseType, releaseName, releaseVersion);
        } else if (StringUtils.isNotBlank(releaseName)) {
            return release(releaseType, releaseName);
        } else if (StringUtils.isNotBlank(releaseVersion)) {
            throw new BadRequestException("Cannot retrieve a release solely by version name");
        } else {
            return getLatestReleaseVersionPackages(releaseType);
        }
    }

    /**
     * Returns a list of Release entities that are considered active.
     * 
     * @return A list ocntaining all active release entities.
     */
    @GET
    @Path("releases/active")
    @Cache(maxAge = CacheControlCommonValues.AGGRESSIVE_CACHE_MAX_AGE)
    public Response activeReleases() {
        return Response.ok(releaseTrackerService.getActiveReleases()).build();
    }

    /**
     * Provides information for a specific package release. The specific packages are determined by using the provided 'releaseType' and
     * 'releaseName' parameters. Returns a Response containing the desired release package if it exists. Throws a
     * {@link BadRequestException} if the releaseType is invalid. Throws a {@link NotFoundException} if the release cannot be found via the
     * Drupal release API. Returns a 503 Service Unavailable if there are issues fetching data from Drupal.
     * 
     * @param releaseType The desired release type.
     * @param releaseName The desired release name.
     * @return A 200 OK Response containing the desired ReleaseVersionPackages entity if it is avaiable. Throws a 400 BadRequest if params
     * are invalid. Throws a 404 NotFound if the specified release package is unavailable.
     */
    @GET
    @Path("releases/{releaseType}/{releaseName}")
    @Cache(maxAge = CacheControlCommonValues.AGGRESSIVE_CACHE_MAX_AGE)
    public Response release(@PathParam("releaseType") String releaseType, @PathParam("releaseName") String releaseName) {
        // Ensure release type is valid
        if (!isValidType(releaseType)) {
            throw new BadRequestException(String.format(RELEASE_TYPE_ERROR_MESSAGE_FORMAT, releaseType));
        }

        // check that the release name is valid
        Optional<Release> r = releaseTrackerService.getReleaseByName(releaseName);
        if (r.isEmpty()) {
            throw new NotFoundException(String.format("No release named '%s' found in store, cannot continue", releaseName));
        }

        // get all of the release version packages to return
        try {
            return Response
                    .ok(r
                            .get()
                            .getVersions()
                            .stream()
                            .map(v -> ReleaseVersionPackages
                                    .builder()
                                    .setPackages(api.get(releaseName, v.getName()))
                                    .setReleaseName(releaseName)
                                    .setReleaseVersion(v.getName())
                                    .build())
                            .toList())
                    .build();
        } catch (WebApplicationException e) {
            String message = String
                    .format("Error while retrieving versioned packages for release '%s': %s", releaseName, e.getLocalizedMessage());
            LOGGER.error(message);
            return new WebError(Status.SERVICE_UNAVAILABLE, message).asResponse();
        }
    }

    /**
     * Provides information for a specific package release. The specific packages are determined by using the provided 'releaseType',
     * 'releaseName', and 'releaseVersion' parameters. Returns a Response containing the desired release package if it exists. Throws a
     * {@link BadRequestException} if the releaseType is invalid or if the releaseVersion is provided without the releaseName. Throws a
     * {@link NotFoundException} if the release cannot be found via the Drupal release API or if the specific version isn't among the Drupal
     * response. Returns a 503 Service Unavailable if there are issues fetching data from Drupal.
     * 
     * @param releaseType The desired release type.
     * @param releaseName The desired release name.
     * @param releaseVersion The desired release version.
     * @return A 200 OK Response containing the desired ReleaseVersionPackages entity if it is avaiable. Throws a 400 BadRequest if params
     * are invalid. Throws a 404 NotFound if the specified release package is unavailable.
     */
    @GET
    @Path("releases/{releaseType}/{releaseName}/{releaseVersion}")
    @Cache(maxAge = CacheControlCommonValues.AGGRESSIVE_CACHE_MAX_AGE)
    public Response releaseVersion(@PathParam("releaseType") String releaseType, @PathParam("releaseName") String releaseName,
            @PathParam("releaseVersion") String releaseVersion) {
        // Ensure release type is valid
        if (!isValidType(releaseType)) {
            throw new BadRequestException(String.format(RELEASE_TYPE_ERROR_MESSAGE_FORMAT, releaseType));
        }

        // check that the release name is valid
        Optional<Release> r = releaseTrackerService.getReleaseByName(releaseName);
        if (r.isEmpty()) {
            throw new NotFoundException(String.format("No release named '%s' found in store, cannot continue", releaseName));
        }

        // check if there is a release version that matches the given version
        if (r.get().getVersions().stream().noneMatch(rp -> rp.getName().equals(releaseVersion))) {
            throw new NotFoundException(String.format("No version named '%s' found for release named '%s'", releaseVersion, releaseName));
        }

        // get the release package from the Drupal API
        Optional<ReleaseTrackerPackages> rtp = getVersionPackages(releaseName, releaseVersion);
        if (rtp.isEmpty()) {
            String message = String
                    .format("Could not find package definitions for release '%s', version '%s'", releaseName, releaseVersion);
            LOGGER.error(message);
            return new WebError(Status.SERVICE_UNAVAILABLE, message).asResponse();
        }
        return Response
                .ok(ReleaseVersionPackages
                        .builder()
                        .setPackages(rtp.get())
                        .setReleaseName(releaseName)
                        .setReleaseVersion(releaseVersion)
                        .build())
                .build();
    }

    /**
     * Iterates over the current active releases to find the most recent one, and then converts it to the release packages output.
     * 
     * @param releaseType the release type for the current request
     * @return response containing the most recent release packages
     */
    private Response getLatestReleaseVersionPackages(String releaseType) {
        // set up the reused predicates
        Predicate<ReleaseVersion> isLatestReleaseVersion = rv -> rv.getIsCurrent() && "release".equalsIgnoreCase(rv.getType());
        Supplier<ServerErrorException> errorForMissingCurrentRelease = () -> new ServerErrorException(
                "Could not find any active modern releases", Status.INTERNAL_SERVER_ERROR);
        // get the latest modern release
        Release latestRelease = releaseTrackerService
                .getActiveReleases()
                .stream()
                .filter(release -> release.getVersions().stream().anyMatch(isLatestReleaseVersion)
                        && MODERN_RELEASE_PATTERN.matcher(release.getName()).find())
                .sorted((r1, r2) -> r2.getName().compareTo(r1.getName()))
                .findFirst()
                .orElseThrow(errorForMissingCurrentRelease);
        // convert the data to the release version output
        return releaseVersion(releaseType, latestRelease.getName(),
                latestRelease
                        .getVersions()
                        .stream()
                        .filter(isLatestReleaseVersion)
                        .findFirst()
                        .orElseThrow(errorForMissingCurrentRelease)
                        .getName());
    }

    /**
     * @param type the type of the release
     * @return True if valid, false if not
     */
    private boolean isValidType(String type) {
        return "epp".equalsIgnoreCase(type) || "eclipse_packages".equalsIgnoreCase(type);
    }

    /**
     * Get a cached version of the release packages from the API.
     * 
     * @param releaseName the name of the release
     * @param version the version name of the specific release
     * @return
     */
    private Optional<ReleaseTrackerPackages> getVersionPackages(String releaseName, String releaseVersion) {
        return cache
                .get(releaseName + releaseVersion, new MultivaluedHashMap<>(), ReleaseTrackerPackages.class,
                        () -> api.get(releaseName, releaseVersion))
                .data();
    }
}
