CREATE DATABASE eclipse;
USE eclipse;

CREATE TABLE `download_file_index` (
  `file_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `file_name` varchar(255) NOT NULL DEFAULT '',
  `download_count` int(10) unsigned NOT NULL DEFAULT 0,
  `timestamp_disk` bigint(20) NOT NULL DEFAULT 0,
  `size_disk_bytes` bigint(20) NOT NULL DEFAULT 0,
  `md5sum` char(32) DEFAULT NULL,
  `sha1sum` char(40) DEFAULT NULL,
  `sha512sum` char(128) DEFAULT NULL,
  PRIMARY KEY (`file_id`),
  KEY `IDX_timestamp_disk` (`timestamp_disk`),
  KEY `idx_file_name` (`file_name`(30))
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;