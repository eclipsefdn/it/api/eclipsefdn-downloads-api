SHELL = /bin/bash

pre-setup:;
	@echo "Creating environment file from template"
	@rm -f .env && envsubst < config/.env.sample | tr -d '\r' > .env

setup:;
	@echo "Generating secret files from templates using environment file + variables"
	@source .env && rm -f ./config/application/secret.properties && envsubst < config/application/secret.properties.sample | tr -d '\r' > config/application/secret.properties

dev-start:;
	source .env && mvn compile quarkus:dev -Dconfig.secret.path=$$PWD/config/application/secret.properties

clean:;
	mvn clean
	yarn run clean

compile-java: compile-test-resources;
	mvn compile package

compile-java-quick: compile-test-resources;
	mvn compile package -Dmaven.test.skip=true
	
compile-micro: compile-test-resources;
	mvn package -Dnative

compile: clean compile-java;

compile-quick: clean compile-java-quick;

install-yarn:;
	yarn install --frozen-lockfile --audit

compile-test-resources: install-yarn;
	yarn run generate-json-schema

compile-start: compile-java;
	docker build -f src/main/docker/Dockerfile.jvm -t local/eclipsefdn-downloads-api:latest .
	docker compose --profile=jvm down
	docker compose build
	docker compose --profile=jvm up -d
	
compile-start-micro: compile-micro;
	docker build -f src/main/docker/Dockerfile.native-micro -t local/eclipsefdn-downloads-api:micro-latest .
	docker compose --profile=native down
	docker compose build
	docker compose --profile=native up -d

start-spec: compile-test-resources;
	yarn run start

generate-notice-src:;
	docker run -v $(PWD)/:/project \
		-v /project/node_modules \
		-v /project/volumes \
		scancode-toolkit -clpeui --json-pp /project/result.json /project