# eclipsefdn-downloads-api

## Getting Started

### Requirements

* Docker
* Java 11 >  
* mvn
* node.js + npm  
* make
* Running MariaDB instance

### Setup

1. In MariaDB instance, create a new database called `eclipse` as that is what is configured out of the box. The table definitions for the database can be found under `./config/mariadb/ddl.sql`.
2. Run `make pre-setup`. This command will setup a basic .env file, using some basic variables used in Eclipse development to accelerate the generation of secret files.
3. Ensure that the `.env` file properties reflect the connection settings used for the MariaDB instance and the credentials needed to access the DB created in step 1.
4. Run `make setup` to finish the setup process for this API, generating the secret file under `./config/application/secret.properties`.

The application can be checked by visiting <http://localhost:8090/downloads/releases/2022-03>. At this address, a return should be visible that represents the 2022-03 release of the Eclipse IDE.

#### Build and Start Server

```bash
make compile-start
```

#### Live Coding Dev Mode

```bash
make dev-start
```

#### Generate Spec

```bash
make generate-test-resources
```

#### Running Tests

```bash
mvn test
```

#### Render a Live UI Preview of the API Spec

```bash
make start-spec
```

## Updating notice

Notice file contents were authored using the output of the Scancode-toolkit, running against the local environment. Any new packages added to the project should be evaluated before they are added to the project to ensure they are compatible with the EPL-2.0 license that this API is available under. To make use of Scancode, it will need to be [installed for Docker](https://scancode-toolkit.readthedocs.io/en/latest/getting-started/install.html#installation-via-docker), and its recommended that the [Scancode-Workbench](https://github.com/nexB/scancode-workbench) be used to evaluate the results.

## Contributing

1. [Fork](https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html) the [eclipsefdn-downloads-api](https://gitlab.eclipse.org/eclipsefdn/it/api/eclipsefdn-downloads-api) repository
2. Clone repository `git clone https://gitlab.eclipse.org/[your_eclipsefdn_username]/eclipsefdn-downloads-api.git`
3. Create your feature branch: `git checkout -b my-new-feature`
4. Commit your changes: `git commit -m 'Add some feature' -s`
5. Push feature branch: `git push origin my-new-feature`
6. Submit a merge request

### Declared Project Licenses

This program and the accompanying materials are made available under the terms
of the Eclipse Public License v. 2.0 which is available at
<http://www.eclipse.org/legal/epl-2.0>.

SPDX-License-Identifier: EPL-2.0
